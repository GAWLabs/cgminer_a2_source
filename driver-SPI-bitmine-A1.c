/*
 * Copyright 2013 Zefir Kurtisi <zefir.kurtisi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.  See COPYING for more details.
 */

#include "config.h"

#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <stdbool.h>
#ifdef HAVE_CURSES
#include <curses.h>
#endif

#include "bcm2835.h"
#include "logging.h"
#include "miner.h"

#include "spi-context.h"

//#define USE_ST_MCU

#ifdef USE_ST_MCU

//features
#define ST_MCU_EN //using ST MCU to control ASIC
#define A1_PLL_CLOCK_EN //config A1's clip PLL clock

#define API_STATE_EN //support show extra ASIC information
#define A1_SPI_SPEED_EN //support change Master SPI speed
#define A1_DIFFICULTY_EN
#define HW_RESET_EN  // hardware reset to STMCU
#define TEST_PIN_EN  // using test pin to test somethings

#define A1_CHK_CMD_TM //check command timeout

#define A1_TEST_MODE_EN 


#define A1_TEMP_EN //support read temperature from ASIC
#ifdef A1_TEMP_EN 
#define A1_CUTOFF_TEMP_CHIP  // don't send job when higher than cutoff temperature
#define A1_SHUTDOWN_ASIC_BD_1  // shutdown ASIC when sigle chip higher than cutoff temperature > 15
#define A1_SHUTDOWN_ASIC_BD_2  // shutdown ASIC when more chips higher than cutoff temperature > 10
#endif

#define A1_DELAY_RESET 1000 // 500 ms
#define A1_MAX_SPEED 12.4

#ifdef HW_RESET_EN
#define ASIC_RESET_PIN	RPI_V2_GPIO_P1_05
#endif

#endif //USE_ST_MCU

#define MAX_POLL_NUM   20 // 2


//#define TEST_DELAY
#define A1_ALL_DELAY 2 // 2 sec
#define MAX_ASIC_BOARD   6

#define MAX_JOB  4
//#define MAX_CORE_NUMS 32
#define MAX_CORE_NUMS 54	// modified for a2
#define MAX_ASIC_NUMS 8 // Max ASIC chip in a chain



#define TEST_PIN  	RPI_V2_GPIO_P1_16



#ifdef ST_MCU_EN
#define ST_MCU_NULL   0
#define ST_MCU_PRE_HEADER 1
static uint8_t stMcu=ST_MCU_NULL;
static int32_t stTempCut=-1;
static int testMode=-1;
static bool testFirstTime=true;

#endif
#define PRE_HEADER  0xb5 // send Pre-header first



/********** work queue */
struct work_ent {
	struct work *work;
	struct list_head head;
};

struct work_queue {
	int num_elems;
	struct list_head head;
};


static bool wq_enqueue(struct work_queue *wq, struct work *work)
{
	if (work == NULL)
		return false;
	struct work_ent *we = malloc(sizeof(*we));
	if (we == NULL)
		return false;
	we->work = work;
	INIT_LIST_HEAD(&we->head);
	list_add_tail(&we->head, &wq->head);
	wq->num_elems++;
	return true;
}

static struct work *wq_dequeue(struct work_queue *wq)
{
	if (wq == NULL)
		return NULL;
	if (wq->num_elems == 0)
		return NULL;
	struct work_ent *we;
	we = list_entry(wq->head.next, struct work_ent, head);
	struct work *work = we->work;

	list_del(&we->head);
	free(we);
	wq->num_elems--;
	return work;
}

/********** chip and chain context structures */
#ifdef A1_TEST_MODE_EN
#define ASIC_BOARD_OK 			0x00000000
//#define NO_ASIC_BOARD 			0x00000010
//#define ASIC_BOARD_TESTING		0x00000001
#define ERROR_CHIP	 			0x00000001
#define ERROR_CORE 				0x00000002
#define ERROR_TEMP_SENSOR	 	0x00000004
#define ERROR_BOARD			 	0x00000008
#define ERROR_OVERHEAD			 	0x00000010


//#define ERROR_OVERHEAT		 	0x00000008

#endif

#ifdef A1_TEMP_EN	
struct Alarm {
	struct timeval tv_start; //alarm start time
	int overheat; //overheat alarm
	int timeout; //sec
	int cutoffTemp; //cutoff temperature 	
};
#endif	

struct A1_chip {
	int num_cores;
	int last_queued_id;
	struct work *work[MAX_JOB];
	/* stats */
	int hw_errors;
	int stales;
	int nonces_found;
	int nonce_ranges_done;
	int reset_errors;
	//uint8_t reg[6]; //latest update from ASIC
	uint8_t reg[12]; //latest update from ASIC	// modified for a2
#ifdef A1_TEMP_EN	
	struct Alarm alarm[2]; // two temp alarm mode
	int temp; //temperature
#endif	

};
#if 0
struct timeval {
	__kernel_time_t		tv_sec;		/* seconds */
	__kernel_suseconds_t	tv_usec;	/* microseconds */
};
#endif
struct A1_chain {
	struct cgpu_info *cgpu;
	int num_chips;
	int num_cores;
	uint8_t spi_tx[128];
	uint8_t spi_rx[128];
#ifdef A1_PLL_CLOCK_EN 	
	uint16_t regPll1;
	uint16_t regPll2;
	uint16_t regPll1to2;
#endif

	struct spi_ctx *spi_ctx;
	struct A1_chip *chips;
	pthread_mutex_t lock;

	struct work_queue active_wq;
#ifdef A1_CHK_CMD_TM
	struct timeval tvStart;
	struct timeval tvEnd;
	struct timeval tvDiff;
#endif
	struct timeval tvScryptLast;
	struct timeval tvScryptCurr;
	struct timeval tvScryptDiff;
	int reset_bc;
	int hashes_times;
	int reinit;
#ifdef A1_TEMP_EN	
	int cutTemp; //cutoff temperature 
	struct Alarm alarm;
	int shutdown;
#endif
#ifdef A1_TEST_MODE_EN
	uint32_t status;
#endif
};

enum A1_command {
	A1_BIST_START		= 0x01,
	A1_BIST_COLLECT		= 0x0b,		// modified for a2
	A1_BIST_FIX		= 0x03,
	A1_RESET		= 0x04,
	A1_RESETBC		= 0x05,		// modified for a2
	A1_WRITE_JOB		= 0x07,
	A1_READ_RESULT		= 0x08,
	A1_WRITE_REG		= 0x09,
	A1_READ_REG		= 0x0a,
	A1_READ_REG_RESP	= 0x1a,
};

int A1_ConfigA1PLLClock(int optPll);
void A1_SetA1PLLClock(struct A1_chain *a1,int pllClkIdx);
static bool cmd_WRITE_REG(struct A1_chain *a1, uint8_t chip, uint8_t *reg);
static void A1_reinit_device(struct cgpu_info *cgpu);
void A1_reinit(void);	
void A1_flush_work_unlock(struct cgpu_info *cgpu);

/********** temporary helper for hexdumping SPI traffic */
#define DEBUG_HEXDUMP 0
static void hexdump(char *prefix, uint8_t *buff, int len)
{
#if DEBUG_HEXDUMP
	static char line[2048];
	char *pos = line;
	int i;
	if (len < 1)
		return;

	pos += sprintf(pos, "%s: %d bytes:", prefix, len);
	for (i = 0; i < len; i++) {
		if (i > 0 && (i % 32) == 0)
			pos += sprintf(pos, "\n\t");
		pos += sprintf(pos, "%.2X ", buff[i]);
	}
	applog(LOG_NOTICE, "%s", line);
	//printf("%s\n\t", line);
#endif
}

#ifdef A1_PLL_CLOCK_EN

#define A1_PLL_POSTDIV_MASK		0b11
#define A1_PLL_PREDIV_MASK		0b11111
#define A1_PLL_FBDIV_H_MASK		0b111111111

// Register masks for A1
//Reg32 Upper
#define A1_PLL_POSTDIV							(46-32)	// (2) pll post divider
#define A1_PLL_PREDIV							(41-32)	// (5) pll pre divider
#define A1_PLL_FBDIV_H							(32-32)	// (9) pll fb divider
//Reg32 Lower
#define A1_PLL_PD								25	// (1) PLL Reset
#define A1_PLL_LOCK								24	// (1) PLL Lock status (1: PLL locked , 0: PLL not locked )
#define A1_INTERNAL_SPI_FREQUENCY_CONFIG		18 	// (1) 1:  Internal  SPI  frequency  is System  clock  frequency  divide 64 
													//      0:  Internal  SPI  frequency  is System  clock  frequency  divide 128

#define A1_BACKUP_QUEUE_FULL_FLAG				17	// (1) 1:Backup queue is full , 0: Backup queue empty 
#define A1_ACTIVE_QUEUE_BUSY_FLAG				16	// (1) 1:Current active queue busy, 0:  Current  active  queue  not busy 
#define A1_BACKUP_QUEUE_JOB_ID					12	// (4) Job  id  corresponding  with  the backup queue  
#define A1_ACTIVE_QUEUE_JOB_ID					8	// (4) Job  id  corresponding  with  the current active queue 
#define A1_GOOD_CORE_NO       					0 	// (8) Good cores inside the chip, 32 maximum

typedef enum
{
    	A1_PLL_CLOCK_1400MHz = 0,
    	A1_PLL_CLOCK_1300MHz,
    	A1_PLL_CLOCK_1280MHz,
    	A1_PLL_CLOCK_1200MHz,
    	A1_PLL_CLOCK_1100MHz,
    	A1_PLL_CLOCK_1080MHz,
    	A1_PLL_CLOCK_1000MHz,
	A1_PLL_CLOCK_MAX,
} A1_PLL_CLOCK;

static uint8_t A1Pll1=A1_PLL_CLOCK_1000MHz;
static uint8_t A1Pll2=A1_PLL_CLOCK_1000MHz;
static uint8_t A1Pll3=A1_PLL_CLOCK_1000MHz;
static uint8_t A1Pll4=A1_PLL_CLOCK_1000MHz;
static uint8_t A1Pll5=A1_PLL_CLOCK_1000MHz;
static uint8_t A1Pll6=A1_PLL_CLOCK_1000MHz;

struct PLL_Clock {
	uint32_t speedMHz;  	// unit MHz
	uint32_t hastRate;  	// divider 1000
	uint16_t pll1_reg;	// pll1_postdiv(2)|pll1_prediv(5)|pll1_fbdiv(9)
	uint16_t pll2_reg;	// pll2_postdiv(2)|pll2_prediv(5)|pll2_fbdiv(9)
	uint16_t pll1to2_reg;	// pll1to2_postdiv(2)
};

#define A1_PLL(postdiv,prediv,fbdiv) ((postdiv<<14)|(prediv<<9)|fbdiv)
const struct PLL_Clock PLL_Clk_12Mhz[A1_PLL_CLOCK_MAX]={
	{1400,2170,A1_PLL(0b01,0b00011,0b101011110),A1_PLL(0b01,0b01000,0b000100000),0b10},
	{1300,2015,A1_PLL(0b01,0b00011,0b101000101),A1_PLL(0b01,0b01000,0b000100000),0b10},
	{1280,1984,A1_PLL(0b01,0b00011,0b101000000),A1_PLL(0b01,0b01000,0b000100000),0b10},
	{1200,1860,A1_PLL(0b01,0b00011,0b100101100),A1_PLL(0b01,0b00100,0b000010000),0b10},
	{1100,1705,A1_PLL(0b01,0b00011,0b100010011),A1_PLL(0b01,0b00100,0b000010000),0b10},
	{1080,1674,A1_PLL(0b01,0b00011,0b100001110),A1_PLL(0b01,0b01000,0b000100000),0b10},
	{1000,1550,A1_PLL(0b01,0b00011,0b011111010),A1_PLL(0b01,0b01000,0b000100000),0b10}
};

	
int A1_ConfigA1PLLClock(int optPll)
{
	int i;
	int A1Pll;
	if(optPll>0)
	{
		A1Pll=A1_PLL_CLOCK_1000MHz; 
		if(optPll<=PLL_Clk_12Mhz[A1_PLL_CLOCK_1000MHz].speedMHz)
		{
			A1Pll=A1_PLL_CLOCK_1000MHz; //found
		}
		else
		{
			for(i=1;i<A1_PLL_CLOCK_MAX;i++){
				if((optPll>PLL_Clk_12Mhz[i].speedMHz)&&(optPll<=PLL_Clk_12Mhz[i-1].speedMHz))
				{	
					A1Pll=i-1; //found
					break;
				}
			}
		}

		applog(LOG_NOTICE, "A1 = %d,%d",optPll,A1Pll);

		//applog(LOG_NOTICE, "A1 PLL Clock = %dMHz, HastRate = %d.%1dGH/s",\
		//	PLL_Clk_12Mhz[A1Pll].speedMHz, \
		//	PLL_Clk_12Mhz[A1Pll].hastRate/10,PLL_Clk_12Mhz[A1Pll].hastRate%10); 
		applog(LOG_NOTICE, "A1 PLL Clock = %dMHz",PLL_Clk_12Mhz[A1Pll].speedMHz);
		}

	return A1Pll;	
}


void A1_SetA1PLLClock(struct A1_chain *a1,int pllClkIdx)
{
	uint8_t i;
	struct A1_chip *chip;
	
	if(pllClkIdx<0||pllClkIdx>A1_PLL_CLOCK_MAX) //out of range
		return;

	a1->regPll1=((((PLL_Clk_12Mhz[pllClkIdx].pll1_reg) >> 8) & 0x00FF) | (((PLL_Clk_12Mhz[pllClkIdx].pll1_reg) << 8) & 0xFF00));
	a1->regPll2=((((PLL_Clk_12Mhz[pllClkIdx].pll2_reg) >> 8) & 0x00FF) | (((PLL_Clk_12Mhz[pllClkIdx].pll2_reg) << 8) & 0xFF00));

	if(a1->chips == NULL)
		return;

	chip = &a1->chips[0];
	memcpy(chip->reg,  (uint8_t*)&a1->regPll1,2);
	memcpy(chip->reg+4,(uint8_t*)&a1->regPll2,2);
	cmd_WRITE_REG(a1, 0, chip->reg);
}
#endif //A1_PLL_CLOCK_EN



/********** upper layer SPI functions */

static bool spi_send_command(struct A1_chain *a1, uint8_t cmd, uint8_t addr,
			     uint8_t *buff, int len)
{
	int tx_len;

if(stMcu==ST_MCU_PRE_HEADER)
{

	memset(a1->spi_tx + 2, 0, len);
	a1->spi_tx[0] = PRE_HEADER;
	a1->spi_tx[1] = PRE_HEADER;

	a1->spi_tx[2] = cmd;
	a1->spi_tx[3] = addr;
	if (len > 0 && buff != NULL)
		memcpy(a1->spi_tx + 4, buff, len);
	tx_len = (4 + len + 1) & ~1;
}
else
{

	memset(a1->spi_tx + 2, 0, len);
	a1->spi_tx[0] = cmd;
	a1->spi_tx[1] = addr;
	if (len > 0 && buff != NULL)
		memcpy(a1->spi_tx + 2, buff, len);
	tx_len = (2 + len + 1) & ~1;

}

	applog(LOG_DEBUG, "Processing command 0x%02x%02x", cmd, addr);
	bool retval = spi_transfer(a1->spi_ctx, a1->spi_tx, a1->spi_rx, tx_len);
	hexdump("TX", a1->spi_tx, tx_len);
	hexdump("RX", a1->spi_rx, tx_len);

	if(retval==false)
		applog(LOG_ERR, "SPI Err:spi_send_command 0x%2x",cmd);

	return retval;
}

static bool spi_poll_result(struct A1_chain *a1, uint8_t cmd,
			    uint8_t chip_id, int len)
{
	int i;
	int pollLen;
	pollLen=MAX_POLL_NUM*a1->num_chips;
	if(pollLen<=0)
		pollLen=MAX_POLL_NUM;
		
	for(i = 0; i < pollLen; i++) {
		bool s = spi_transfer(a1->spi_ctx, NULL, a1->spi_rx, 2);
		hexdump("TX", a1->spi_tx, 2);
		hexdump("RX", a1->spi_rx, 2);
		if (!s)
		{
			applog(LOG_ERR, "SPI(cs%d) Err:spi_poll_result 0x%2x",a1->spi_ctx->config.gpio_cs,cmd);
			return false;
		}
		if (a1->spi_rx[0] == cmd && a1->spi_rx[1] == chip_id) {
			applog(LOG_DEBUG, "Cmd 0x%02x ACK'd", cmd);
			if (len == 0)
				return true;
			s = spi_transfer(a1->spi_ctx, NULL,
					 a1->spi_rx + 2, len);
			hexdump("RX", a1->spi_rx + 2, len);
			if (!s)
			{
				applog(LOG_ERR, "SPI(cs%d) Err:spi_poll_result 0x%2x",a1->spi_ctx->config.gpio_cs,cmd);
				return false;
			}
			hexdump("poll_result", a1->spi_rx, len + 2);
			return true;
		}
//		cgsleep_us(10);

	}
	applog(LOG_WARNING, "Failure(cs%d)(%d): missing ACK for cmd 0x%02x", a1->spi_ctx->config.gpio_cs,pollLen,cmd);
	return false;
}

static bool spi_poll_result_fast(struct A1_chain *a1, uint8_t cmd,
			    uint8_t chip_id, int len,uint32_t timeout)
{
	int i;
	int pollLen;
	bool firstTime=true;
	pollLen=MAX_POLL_NUM*a1->num_chips;
	if(pollLen<=0)
		pollLen=MAX_POLL_NUM;
		
	for(i = 0; i < pollLen; i++) {
		bool s = spi_transfer(a1->spi_ctx, NULL, a1->spi_rx, 2);
		hexdump("TX", a1->spi_tx, 2);
		hexdump("RX", a1->spi_rx, 2);
		if (!s)
		{
			applog(LOG_ERR, "SPI(cs%d) Err:spi_poll_result_fast 0x%2x",a1->spi_ctx->config.gpio_cs,cmd);
			return false;
		}

		if (a1->spi_rx[0] == 0xff&& a1->spi_rx[1] == 0xff)
			{
				applog(LOG_ERR, "SPI(cs%d) no device",a1->spi_ctx->config.gpio_cs);
				return false;
			}

		if (a1->spi_rx[0] == cmd && a1->spi_rx[1] == chip_id) {
			applog(LOG_DEBUG, "Cmd 0x%02x ACK'd", cmd);
			if (len == 0)
				return true;
			s = spi_transfer(a1->spi_ctx, NULL,
					 a1->spi_rx + 2, len);
			hexdump("RX", a1->spi_rx + 2, len);
			if (!s)
			{
				applog(LOG_ERR, "SPI(cs%d) Err2:spi_poll_result_fast 0x%2x",a1->spi_ctx->config.gpio_cs,cmd);
				return false;
			}
			hexdump("poll_result", a1->spi_rx, len + 2);
			return true;
		}
		if(firstTime)
		{
			cgsleep_ms(timeout);
			firstTime=false;
		}
//		cgsleep_ms(10);
//		cgsleep_us(500);
//	cgsleep_us(10);

	}
//	applog(LOG_WARNING, "Failure: missing ACK for cmd 0x%02x", cmd);
	applog(LOG_WARNING, "Failure(cs%d)(%d): missing ACK for cmd 0x%02x", a1->spi_ctx->config.gpio_cs,pollLen,cmd);

	return false;
}

/********** A1 SPI commands */
#ifdef ST_MCU_EN
static bool cmd_BIST_START(struct A1_chain *a1)
{
	bool ret;

#ifdef A1_CHK_CMD_TM
	cgtime(&a1->tvStart);
#endif

	ret=spi_send_command(a1, A1_BIST_START, 0x00, NULL, 0);
//	ret=spi_poll_result_fast(a1, A1_BIST_START, 0x00, 2,A1_DELAY_RESET);
	ret=spi_poll_result(a1, A1_BIST_START, 0x00, 2);
	if(ret==false) 	
	{
		cgtime(&a1->tvEnd);
		timersub(&a1->tvEnd, &a1->tvStart, &a1->tvDiff);
		applog(LOG_WARNING, "ACK(cs%d) timeout:cmd_BIST_START-%d.%04ds",a1->spi_ctx->config.gpio_cs,a1->tvDiff.tv_sec,a1->tvDiff.tv_usec);
	}
	return ret;
}

static bool cmd_BIST_COLLECT_BCAST(struct A1_chain *a1)		// modified for a2
{
	bool ret;

#ifdef A1_CHK_CMD_TM
	cgtime(&a1->tvStart);
#endif


	ret=spi_send_command(a1, A1_BIST_COLLECT, 0x00, NULL, 0);
//	ret=spi_poll_result_fast(a1, A1_BIST_COLLECT, 0x00, 0,A1_DELAY_RESET);
	ret=spi_poll_result(a1, A1_BIST_COLLECT, 0x00, 0);
	if(ret==false) 	
	{
		cgtime(&a1->tvEnd);
		timersub(&a1->tvEnd, &a1->tvStart, &a1->tvDiff);
		applog(LOG_WARNING, "ACK(%d) timeout:cmd_BIST_COLLECT_BCAST-%d.%04ds",a1->spi_ctx->config.gpio_cs,a1->tvDiff.tv_sec,a1->tvDiff.tv_usec);
	}

	return ret;
}

static bool cmd_BIST_FIX_BCAST(struct A1_chain *a1)
{
	bool ret;

#ifdef A1_CHK_CMD_TM
	cgtime(&a1->tvStart);
#endif


	ret=spi_send_command(a1, A1_BIST_FIX, 0x00, NULL, 0);
//	ret=spi_poll_result_fast(a1, A1_BIST_FIX, 0x00, 0,A1_DELAY_RESET);
	ret=spi_poll_result(a1, A1_BIST_FIX, 0x00, 0);
	if(ret==false) 	
	{
		cgtime(&a1->tvEnd);
		timersub(&a1->tvEnd, &a1->tvStart, &a1->tvDiff);
		applog(LOG_WARNING, "ACK(%d) timeout:cmd_BIST_FIX_BCAST-%d.%04ds",a1->spi_ctx->config.gpio_cs,a1->tvDiff.tv_sec,a1->tvDiff.tv_usec);
	}

	return ret;
}

static bool cmd_RESET_BCAST(struct A1_chain *a1)
{
//	struct timeval tv_start, tv_end,tv_diff;

	bool ret;
#ifdef A1_CHK_CMD_TM
	cgtime(&a1->tvStart);
#endif

	ret=spi_send_command(a1, A1_RESET, 0x00, NULL, 0);
#ifdef ST_MCU_EN
	if(ret==false)
	{
		return ret;
	}	

#if 0	
	cgtime(&tv_start);

	cgsleep_ms(A1_DELAY_RESET); 
	cgtime(&tv_end);

	timersub(&tv_end, &tv_start, &tv_diff);

	applog(LOG_WARNING, "Reset time %d\n",tv_diff.tv_sec);
#endif	

#endif

	ret=spi_poll_result_fast(a1, A1_RESET, 0x00, 0,A1_DELAY_RESET);
//	ret=spi_poll_result(a1, A1_RESET, 0x00, 0);

	if(ret==false) 	
	{
		cgtime(&a1->tvEnd);
		timersub(&a1->tvEnd, &a1->tvStart, &a1->tvDiff);
		applog(LOG_WARNING, "ACK(cs%d) timeout:cmd_RESET_BCAST-%d.%04ds",a1->spi_ctx->config.gpio_cs,a1->tvDiff.tv_sec,a1->tvDiff.tv_usec);		
	}


//	if(ret==false) 	applog(LOG_WARNING, "ACK timeout:cmd_RESET_BCAST\n");
	return ret;
}

static bool cmd_RESETBC_BCAST(struct A1_chain *a1)
{
//	struct timeval tv_start, tv_end,tv_diff;

	bool ret;
#ifdef A1_CHK_CMD_TM
	cgtime(&a1->tvStart);
#endif

	ret=spi_send_command(a1, A1_RESETBC, 0x00, NULL, 0);
#ifdef ST_MCU_EN
	if(ret==false)
	{
		return ret;
	}	

#if 0	
	cgtime(&tv_start);

	cgtime(&tv_end);

	timersub(&tv_end, &tv_start, &tv_diff);

	applog(LOG_WARNING, "Reset Bc time %d\n",tv_diff.tv_sec);
#endif	

#endif

	cgsleep_us(10);

//	ret=spi_poll_result_fast(a1, A1_RESETBC, 0x00, 0,A1_DELAY_RESET);
	ret=spi_poll_result(a1, A1_RESETBC, 0x00, 0);

	if(ret==false) 	
	{
		cgtime(&a1->tvEnd);
		timersub(&a1->tvEnd, &a1->tvStart, &a1->tvDiff);
		applog(LOG_WARNING, "ACK(cs%d) timeout:cmd_RESETBC_BCAST-%d.%04ds",a1->spi_ctx->config.gpio_cs,a1->tvDiff.tv_sec,a1->tvDiff.tv_usec);		
	}


//	if(ret==false) 	applog(LOG_WARNING, "ACK timeout:cmd_RESETBC_BCAST\n");
	return ret;
}



static bool cmd_WRITE_REG(struct A1_chain *a1, uint8_t chip, uint8_t *reg)
{
	bool ret;

#if 0 //patch A
	return	spi_send_command(a1, A1_WRITE_REG, chip, reg, 6)&&
		spi_poll_result(a1, A1_WRITE_REG, chip, 6);
#else
	//ret=spi_send_command(a1, A1_WRITE_REG, chip, reg, 6);
	ret=spi_send_command(a1, A1_WRITE_REG, chip, reg, 12);	// modified for a2
	cgsleep_us(10);
	spi_transfer(a1->spi_ctx, NULL, a1->spi_rx, 2);
        hexdump("RX", a1->spi_rx, 2);
#endif
	return ret;
}

static bool cmd_READ_REG(struct A1_chain *a1, uint8_t chip)
{
	bool ret;
#ifdef TEST_PIN_EN
	bcm2835_gpio_fsel(TEST_PIN, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_write(TEST_PIN, HIGH);
#endif

#ifdef A1_CHK_CMD_TM
	cgtime(&a1->tvStart);
#endif

	ret=spi_send_command(a1, A1_READ_REG, chip, NULL, 0);
#ifdef A1_TEMP_EN	
	//ret=spi_poll_result(a1, A1_READ_REG_RESP, chip, 8);
	ret=spi_poll_result(a1, A1_READ_REG_RESP, chip, 14);	// modified for a2
#else
	//ret=spi_poll_result(a1, A1_READ_REG_RESP, chip, 6);
	ret=spi_poll_result(a1, A1_READ_REG_RESP, chip, 12);	// modified for a2
#endif	

	if(ret==false) 	
	{
		cgtime(&a1->tvEnd);
		timersub(&a1->tvEnd, &a1->tvStart, &a1->tvDiff);
		applog(LOG_WARNING, "ACK(cs%d) timeout:cmd_READ_REG-%d.%ds",a1->spi_ctx->config.gpio_cs,a1->tvDiff.tv_sec,a1->tvDiff.tv_usec);
	}
#ifdef TEST_PIN_EN
	bcm2835_gpio_fsel(TEST_PIN, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_write(TEST_PIN, LOW);
#endif


	return ret;
}

#else

static bool cmd_BIST_START(struct A1_chain *a1)
{
	return	spi_send_command(a1, A1_BIST_START, 0x00, NULL, 0) &&
		spi_poll_result(a1, A1_BIST_START, 0x00, 2);
}

static bool cmd_BIST_FIX_BCAST(struct A1_chain *a1)
{
	return	spi_send_command(a1, A1_BIST_FIX, 0x00, NULL, 0) &&
		spi_poll_result(a1, A1_BIST_FIX, 0x00, 0);
}

static bool cmd_RESET_BCAST(struct A1_chain *a1)
{
	return	spi_send_command(a1, A1_RESET, 0x00, NULL, 0) &&
		spi_poll_result(a1, A1_RESET, 0x00, 0);
}

static bool cmd_WRITE_REG(struct A1_chain *a1, uint8_t chip, uint8_t *reg)
{
	return	spi_send_command(a1, A1_WRITE_REG, chip, reg, 6);
}

static bool cmd_READ_REG(struct A1_chain *a1, uint8_t chip)
{
	return	spi_send_command(a1, A1_READ_REG, chip, NULL, 0) &&
		spi_poll_result(a1, A1_READ_REG_RESP, chip, 6);
}
#endif //GPIO_CS_EN


/********** A1 low level functions */
static void A1_hw_reset(void)
{

#ifdef HW_RESET_EN

		applog(LOG_NOTICE, "ST MCU hardware reset start");

		bcm2835_gpio_fsel(ASIC_RESET_PIN, BCM2835_GPIO_FSEL_OUTP);
		bcm2835_gpio_write(ASIC_RESET_PIN, HIGH);
	
		cgsleep_ms(2000);
	
		bcm2835_gpio_fsel(ASIC_RESET_PIN, BCM2835_GPIO_FSEL_OUTP);
		bcm2835_gpio_write(ASIC_RESET_PIN, LOW);
	
		cgsleep_ms(2000);
#else
	/* TODO: issue cold reset */
//	usleep(100000);
#endif
}

static void rev(unsigned char *s, size_t l)
{
	size_t i, j;
	unsigned char t;

	for (i = 0, j = l - 1; i < j; i++, j--) {
		t = s[i];
		s[i] = s[j];
		s[j] = t;
	}
}

/********** job creation and result evaluation */
#ifdef A1_DIFFICULTY_EN

static const uint8_t difficult_Tbl[16][4] = {
	{0x1e, 0xff, 0xff, 0xff},	// 1
	{0x1e, 0x7f, 0xff, 0xff},	// 2
	{0x1e, 0x3f, 0xff, 0xff},	// 4
	{0x1e, 0x1f, 0xff, 0xff},	// 8
	{0x1e, 0x0f, 0xff, 0xff},	// 16
	{0x1e, 0x07, 0xff, 0xff},	// 32
	{0x1e, 0x03, 0xff, 0xff},	// 64 
	{0x1e, 0x01, 0xff, 0xff},	// 128
	{0x1e, 0x00, 0xff, 0xff},	// 256
	{0x1e, 0x00, 0x7f, 0xff},	// 512
	{0x1e, 0x00, 0x3f, 0xff},	// 1024
	{0x1e, 0x00, 0x1f, 0xff},	// 2046
	{0x1e, 0x00, 0x0f, 0xff}	// 4096
};

#endif
static uint8_t *create_job(uint8_t chip_id, uint8_t job_id,
			   const char *wdata, const double sdiff)
{
	//static uint8_t job[58] = {
	static uint8_t job[90] = {			// modified for a2
		/* command */
		0x00, 0x00,
		/* wdata 63 to 0 */
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		/* start nonce */
		0x00, 0x00, 0x00, 0x00,
		/* wdata 75 to 64 */
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		/* difficulty */
		0x00, 0x00, 0x00, 0x00,
		/* end nonce */
		0x00, 0x00, 0x00, 0x00
	};
	
	uint8_t diffIdx;
	
	uint8_t data63to0[64];
	uint8_t startnonce[4] = {0x00, 0x00, 0x00, 0x00};
	uint8_t data75to64[12];
	uint8_t diff[4]       = {0x1e, 0x03, 0xff, 0xff};
	//uint8_t endnonce[4] = {0x00, 0x01, 0x00, 0x00};	//
	//uint8_t endnonce[4] = {0x01, 0x00, 0x00, 0x00};	// 10s
	//uint8_t endnonce[4] = {0x1f, 0xff, 0xff, 0xff};	// 320s
	uint8_t endnonce[4]   = {0xff, 0xff, 0xff, 0xff};	// 46m

	memcpy(data63to0, wdata, 64);
	memcpy(data75to64, wdata+64, 12);
	
	//printf("sdiff = %f\n", sdiff);

#ifdef A1_DIFFICULTY_EN
	if(sdiff > 4095.0)
		memcpy(diff, difficult_Tbl[12], 4);
	else if(sdiff > 2047.0)
		memcpy(diff, difficult_Tbl[11], 4);
	else if(sdiff > 1023.0)
		memcpy(diff, difficult_Tbl[10], 4);
	else if(sdiff > 511.0)
		memcpy(diff, difficult_Tbl[9], 4);
	else if(sdiff > 255.0)
		memcpy(diff, difficult_Tbl[8], 4);
	else {
		if(opt_diff>=1&&opt_diff<=8)
		{
			diffIdx=opt_diff-1;
			memcpy(diff, difficult_Tbl[diffIdx], 4);
		}
		else
			memcpy(diff, difficult_Tbl[7], 4);
	}
#endif
	
	//printf("diff =0x%.2x 0x%.2x 0x%.2x 0x%.2x \n", diff[0], diff[1], diff[2], diff[3]);	

	rev(data63to0, 64);
	rev(startnonce, 4);
	rev(data75to64, 12);
	rev(diff, 4);
	rev(endnonce, 4);

	job[0] = (job_id << 4) | A1_WRITE_JOB;
	job[1] = chip_id;
	memcpy(job+2, 	        data63to0,  64);
	memcpy(job+2+64,        startnonce, 4);
	memcpy(job+2+64+4,      data75to64, 12);
	memcpy(job+2+64+4+12,   diff,       4);
	memcpy(job+2+64+4+12+4, endnonce,   4);	

	return job;
}

/* set work for given chip, returns true if a nonce range was finished */
static bool set_work(struct A1_chain *a1, uint8_t chip_id, struct work *work)
{
	//unsigned char *midstate = work->midstate;
	//unsigned char *wdata = work->data + 64;
	unsigned char *wdata = work->data;		// modified for a2	
	double sdiff = work->sdiff;

	if(a1->chips == NULL)
		return false;

	struct A1_chip *chip = &a1->chips[chip_id - 1];
	bool retval = false;

	chip->last_queued_id++;
	chip->last_queued_id &= 3;

	if (chip->work[chip->last_queued_id] != NULL) {
		work_completed(a1->cgpu, chip->work[chip->last_queued_id]);
		chip->work[chip->last_queued_id] = NULL;
		retval = true;
	}
	uint8_t *jobdata = create_job(chip_id, chip->last_queued_id + 1,
				      wdata, sdiff);

if(stMcu==ST_MCU_PRE_HEADER)
{
	//hexdump("JOB", jobdata, 58);
	hexdump("JOB", jobdata, 90);			// modified for a2
	a1->spi_tx[0]=PRE_HEADER;
	a1->spi_tx[1]=PRE_HEADER;
	//memcpy(&a1->spi_tx[2], jobdata, 58);
	memcpy(&a1->spi_tx[2], jobdata, 90);	// modified for a2
	//if (!spi_transfer(a1->spi_ctx, a1->spi_tx, a1->spi_rx, 60)) {
	if (!spi_transfer(a1->spi_ctx, a1->spi_tx, a1->spi_rx, 92)) {	// modified for a2
		/* give back work */
		work_completed(a1->cgpu, work);

		applog(LOG_ERR, "Failed to set work for chip(cs%d) %d.%d",
		       a1->spi_ctx->config.gpio_cs,chip_id, chip->last_queued_id + 1);
		// TODO: what else?
	} else {
		chip->work[chip->last_queued_id] = work;
	}
}
else
{
	//hexdump("JOB", jobdata, 58);
	hexdump("JOB", jobdata, 90);			// modified for a2
	//memcpy(a1->spi_tx, jobdata, 58);
	memcpy(a1->spi_tx, jobdata, 90);		// modified for a2
	//if (!spi_transfer(a1->spi_ctx, jobdata, a1->spi_rx, 58)) {
	if (!spi_transfer(a1->spi_ctx, jobdata, a1->spi_rx, 90)) {		// modified for a2

		/* give back work */
		work_completed(a1->cgpu, work);

		applog(LOG_ERR, "Failed to set work for chip(cs%d) %d.%d",
		       a1->spi_ctx->config.gpio_cs,chip_id, chip->last_queued_id + 1);
		// TODO: what else?
	} else {
		chip->work[chip->last_queued_id] = work;
	}

}
		
	return retval;
}

/* check for pending results in a chain, returns false if output queue empty */
static bool get_nonce(struct A1_chain *a1, uint8_t *nonce,
		      uint8_t *chip, uint8_t *job_id)
{
	int i;

	int pollLen=0;
	pollLen=MAX_POLL_NUM*a1->num_chips;
	if(pollLen<=0)
		pollLen=MAX_POLL_NUM;


#ifdef A1_CHK_CMD_TM
	cgtime(&a1->tvStart);
#endif
	
	if (!spi_send_command(a1, A1_READ_RESULT, 0x00, NULL, 0))
	{
		return false;
	}

	cgsleep_us(10);

	for(i = 0; i < pollLen; i++) {
		memset(a1->spi_tx, 0, 2);
		if (!spi_transfer(a1->spi_ctx, a1->spi_tx, a1->spi_rx, 2))
			{
				applog(LOG_ERR, "SPI Err(cs%d):get_nonce result",a1->spi_ctx->config.gpio_cs);
			return false;
			}
		hexdump("RX", a1->spi_rx, 2);
		if (a1->spi_rx[0] == A1_READ_RESULT && a1->spi_rx[1] == 0x00) {
			applog(LOG_DEBUG, "Output queue empty");
			return false;
		}
		if ((a1->spi_rx[0] & 0x0f) == A1_READ_RESULT &&
#if 1 //Patch C
		    a1->spi_rx[1] != 0) {
#else		    
		    a1->spi_rx[0] != 0) {
#endif		    
			*job_id = a1->spi_rx[0] >> 4;
			*chip = a1->spi_rx[1];

			if (!spi_transfer(a1->spi_ctx, NULL, nonce, 4))
			{
					applog(LOG_ERR, "SPI Err(cs%d):get_nonce",a1->spi_ctx->config.gpio_cs);
				return false;
			}


			applog(LOG_DEBUG, "Got nonce for chip %d / job_id %d",
			       *chip, *job_id);
			hexdump("nonce", nonce, 4);

			return true;
		}
//nouse		cgsleep_us(50);
	}

	cgtime(&a1->tvEnd);
	timersub(&a1->tvEnd, &a1->tvStart, &a1->tvDiff);
	applog(LOG_WARNING, "ACK(cs%d) timeout:get_nonce-%d.%04ds",a1->spi_ctx->config.gpio_cs,a1->tvDiff.tv_sec,a1->tvDiff.tv_usec);
	return false;
}

/* reset input work queues in chip chain */
static bool abort_work(struct A1_chain *a1)
{
	/*
	 * for now, the proposed input queue reset does not seem to work
	 * TODO: implement reliable abort method
	 * NOTE: until then, we are completing queued work => stales
	 */

	a1->reset_bc=1;	

	applog(LOG_DEBUG, "abort_work...");
	return true;
}

/********** driver interface */
void exit_A1_chain(struct A1_chain *a1)
{
	if (a1 == NULL)
		return;
	
	if(a1->chips)
	free(a1->chips);
	a1->chips = NULL;
	spi_exit(a1->spi_ctx);
	a1->spi_ctx = NULL;
	free(a1);
}
#ifdef A1_TEST_MODE_EN //assume ST_MCU_EN enable
struct A1_chain *init_Test_A1_chain(struct spi_ctx *ctx)
{
	int i;//,num_chips =0;
	struct A1_chain *a1 = malloc(sizeof(*a1));
	applog(LOG_DEBUG, "A1 init chain");
	if (a1 == NULL) {
		applog(LOG_ERR, "A1_chain allocation error");
		goto failure;
	}
	memset(a1, 0, sizeof(*a1));
	a1->spi_ctx = ctx;
	a1->num_chips = 0;
	a1->num_cores = 0;	
	a1->chips = NULL;
	mutex_init(&a1->lock);
	INIT_LIST_HEAD(&a1->active_wq.head);

	return a1;

failure:
	if(a1->chips)
	free(a1->chips);
	a1->chips = NULL;
	spi_exit(a1->spi_ctx);
	a1->spi_ctx = NULL;

	return a1;
}

#endif // A1_TEST_MODE_EN //assume ST_MCU_EN enable

struct A1_chain *init_A1_chain(struct spi_ctx *ctx)
{
	int i,num_chips =0;
	struct A1_chain *a1 = malloc(sizeof(*a1));
	applog(LOG_DEBUG, "A1 init chain");
	if (a1 == NULL) {
		applog(LOG_ERR, "A1_chain allocation error");
		goto failure;
	}
	memset(a1, 0, sizeof(*a1));
	a1->spi_ctx = ctx;
	a1->status=ASIC_BOARD_OK;
	a1->shutdown=0;
	a1->reset_bc=1;	// clear old jobs
	a1->reinit=0;

#ifdef ST_MCU_EN

	if (!cmd_RESET_BCAST(a1))
		goto failure;

	if (!cmd_BIST_START(a1))
		goto failure;
#else

	if (!cmd_RESET_BCAST(a1) || !cmd_BIST_START(a1))
		goto failure;
#endif
	a1->num_chips = a1->spi_rx[3];
	applog(LOG_WARNING, "spidev%d.%d(cs%d): Found %d A1 chips",
	       a1->spi_ctx->config.bus, a1->spi_ctx->config.cs_line,
	       a1->spi_ctx->config.gpio_cs,a1->num_chips);

	a1->num_cores=0;

	if (a1->num_chips == 0)
		goto failure;

	a1->chips = calloc(a1->num_chips, sizeof(struct A1_chip));
	if (a1->chips == NULL) {
		applog(LOG_ERR, "A1_chips allocation error");
		goto failure;
	}

	if (!cmd_BIST_COLLECT_BCAST(a1))	// modified for a2
		goto failure;

	if (!cmd_BIST_FIX_BCAST(a1))
		goto failure;

	if(a1->num_chips<MAX_ASIC_NUMS)
		a1->status|=ERROR_CHIP;

	for (i = 0; i < a1->num_chips; i++) {
		int chip_id = i + 1;
		if (!cmd_READ_REG(a1, chip_id)) {
			applog(LOG_WARNING, "Failed to read register for "
			       "chip %d -> disabling", chip_id);
			a1->chips[i].num_cores = 0;
			continue;
		}
		num_chips++;
		//a1->chips[i].num_cores = a1->spi_rx[7];
		a1->chips[i].num_cores = a1->spi_rx[13];		// modified for a2
		//if(a1->spi_rx[7]>MAX_CORE_NUMS)
		if(a1->chips[i].num_cores>MAX_CORE_NUMS)		// modified for a2
			a1->chips[i].num_cores = MAX_CORE_NUMS;

		if(a1->chips[i].num_cores<MAX_CORE_NUMS)
			a1->status=ERROR_CORE;

		//memcpy(a1->chips[i].reg, a1->spi_rx + 2, 6);	//keep ASIC register value
		memcpy(a1->chips[i].reg, a1->spi_rx + 2, 12);	//keep ASIC register value	// modified for a2
#ifdef A1_TEMP_EN	
		//a1->chips[i].temp= (a1->spi_rx[8]<<8)|a1->spi_rx[9];
		a1->chips[i].temp= (a1->spi_rx[14]<<8)|a1->spi_rx[15];	// modified for a2
#endif

#ifdef A1_SHUTDOWN_ASIC_BD_1
		a1->chips[i].alarm[0].overheat=0;
		a1->chips[i].alarm[0].timeout=5;
		a1->chips[i].alarm[0].cutoffTemp=stTempCut+15;

#endif
		
#ifdef A1_SHUTDOWN_ASIC_BD_2
		a1->chips[i].alarm[1].overheat=0;
		a1->chips[i].alarm[1].timeout=5;
		a1->chips[i].alarm[1].cutoffTemp=stTempCut+10;
#endif

		a1->num_cores += a1->chips[i].num_cores;
		applog(LOG_WARNING, "Found chip %d with %d active cores",
		       chip_id, a1->chips[i].num_cores);

		a1->chips[i].reset_errors = 0;
	}
	applog(LOG_WARNING, "Found %d chips with total %d active cores",
	       a1->num_chips, a1->num_cores);

	if(a1->num_cores==0) // The A1 board  haven't any cores
		goto failure;		

	mutex_init(&a1->lock);
	INIT_LIST_HEAD(&a1->active_wq.head);

	return a1;

failure:

	a1->status=ERROR_BOARD;

	exit_A1_chain(a1);
	return NULL;
}

static bool A1_detect_one_chain(struct spi_config *cfg)
{
	struct cgpu_info *cgpu;
	struct spi_ctx *ctx = spi_init(cfg);
	struct A1_chain *a1;
	int i;

	if (ctx == NULL)
		return false;
#ifdef A1_TEST_MODE_EN
	if(testMode>0)
		a1 = init_Test_A1_chain(ctx);
	else
#endif
		a1 = init_A1_chain(ctx);

	if (a1 == NULL)
		return false;

	cgpu = malloc(sizeof(*cgpu));
	if (cgpu == NULL) {
		applog(LOG_ERR, "cgpu allocation error");
		exit_A1_chain(a1);
		return false;
	}
	memset(cgpu, 0, sizeof(*cgpu));
	cgpu->drv = &bitmineA1_drv;
	cgpu->name = "BitmineA1";
	cgpu->threads = 1;

	cgpu->device_data = a1;

#ifdef A1_TEST_MODE_EN
	if(testMode)
		cgpu->deven = DEV_DISABLED;
	else
#endif
	cgpu->deven = DEV_ENABLED;


	a1->cgpu = cgpu;
#ifdef USE_ST_MCU	
	cgpu->restart= 0;
#endif
	a1->cutTemp= stTempCut;


	add_cgpu(cgpu);
#ifdef A1_PLL_CLOCK_EN
	switch(cgpu->device_id){
		case 0:A1_SetA1PLLClock(a1, A1Pll1);break;
		case 1:A1_SetA1PLLClock(a1, A1Pll2);break;
		case 2:A1_SetA1PLLClock(a1, A1Pll3);break;
		case 3:A1_SetA1PLLClock(a1, A1Pll4);break;
		case 4:A1_SetA1PLLClock(a1, A1Pll5);break;
		case 5:A1_SetA1PLLClock(a1, A1Pll6);break;
		default:;
	}
#endif
	return true;
}

#define MAX_SPI_BUS	1
//#define MAX_SPI_CS	2
#define MAX_SPI_CS	1

/* Probe SPI channel and register chip chain */
void A1_detect(bool hotplug)
{
	int bus;
	int cs_line;
//	uint32_t speed=DEFAULT_SPI_SPEED; // 1 500 000;
	uint32_t speed=4000000;  // 4Mhz
//	uint32_t speed=8000000;  // 8Mhz
	uint8_t no_cgpu=0;

	if(!CheckForAnotherInstance())
	{
        pabort("Another cgminer is running");
		return;
	}
	
	/* no hotplug support for now */
	if (hotplug)
		return;
	applog(LOG_DEBUG, "A1 detect");

	gpio_CS_Init();
	
#ifdef A1_TEST_MODE_EN
	if(opt_test>0)
	{
		testMode=opt_test;
		applog(LOG_NOTICE, "Run Test Mode");

	}
#endif
	applog(LOG_NOTICE, "Run Reset=%d",opt_hwReset);

	if(opt_hwReset==true)
	A1_hw_reset();

#ifdef A1_SPI_SPEED_EN
	if(opt_spiSpeed>0)
		speed=opt_spiSpeed*100000; // x 1kHz
	applog(LOG_NOTICE, "SPI Speed %d kHz",speed/1000);

#endif

#ifdef ST_MCU_EN
	if(opt_stmcu<0)
	{
		stMcu=ST_MCU_NULL;
		applog(LOG_NOTICE, "ST MCU - Disable");		
	}
	else
	{
		stMcu=ST_MCU_PRE_HEADER;
		applog(LOG_NOTICE, "ST MCU - Enable (Pre-header)");		
	}
#endif

#ifdef A1_TEMP_EN	
	if(opt_tempCut>0)
	{
		stTempCut=opt_tempCut;
		applog(LOG_NOTICE, "Cutoff temperature %dC",stTempCut);		
	}
#endif

#ifdef A1_PLL_CLOCK_EN
		A1Pll1 = A1_ConfigA1PLLClock(opt_A1Pll1);
		A1Pll2 = A1_ConfigA1PLLClock(opt_A1Pll2);
		A1Pll3 = A1_ConfigA1PLLClock(opt_A1Pll3);
		A1Pll4 = A1_ConfigA1PLLClock(opt_A1Pll4);
		A1Pll5 = A1_ConfigA1PLLClock(opt_A1Pll5);
		A1Pll6 = A1_ConfigA1PLLClock(opt_A1Pll6);
#endif

	//const int diff_table[16]={1,2,3,4,8,16,32,37,43,52,65,86,103,129,256,1024};

	//if(opt_diff>=1&&opt_diff<=16)
	//	applog(LOG_NOTICE, "ASIC Difficulty =  %d",diff_table[opt_diff-1]);		
	//else
	//	applog(LOG_NOTICE, "ASIC Difficulty =  %d",256); //default

#ifdef USE_GPIO_CS 
	uint8_t gpio_cs,minGpioCS,maxGpioCS;
	struct spi_config cfg = default_spi_config;


#ifdef A1_TEST_MODE_EN
	if(testMode>0)
		opt_gcs=G_CS_MAX; //force auto CS if test mode
#endif
	if(opt_gcs<G_CS_MAX)
	{
		if(opt_gcs<0)
		{
			minGpioCS=G_CS_0;
			maxGpioCS=G_CS_0+1;
			applog(LOG_NOTICE, "GPIO CS is OFF");
		}
		else
		{
			minGpioCS=opt_gcs;
			maxGpioCS=opt_gcs+1;
			applog(LOG_NOTICE, "GPIO CS is ON at CS%d",opt_gcs);		
		}
	}
	else //auto >=G_CS_MAX
	{
		minGpioCS=G_CS_0;
		maxGpioCS=MAX_ASIC_BOARD;
		applog(LOG_NOTICE, "AUTO GPIO CS");
	}

	for (gpio_cs = minGpioCS; gpio_cs < maxGpioCS; gpio_cs++) {
		for (bus = 0; bus < MAX_SPI_BUS; bus++) {
			for (cs_line = 0; cs_line < MAX_SPI_CS; cs_line++) {
//				if(gpio_cs ==G_CS_6) //hardware issue skip CS 6
//					continue;
				cfg = default_spi_config;
				cfg.mode = SPI_MODE_1;
				cfg.speed = speed;
				cfg.bus = bus;
				cfg.cs_line = cs_line;
				cfg.gpio_cs = gpio_cs;
				if(A1_detect_one_chain(&cfg))
					no_cgpu++;
			}
		}
	}

#else
	for (bus = 0; bus < MAX_SPI_BUS; bus++) {
		for (cs_line = 0; cs_line < MAX_SPI_CS; cs_line++) {
			struct spi_config cfg = default_spi_config;
			cfg.mode = SPI_MODE_1;
			cfg.speed = speed;
			cfg.bus = bus;
			cfg.cs_line = cs_line;
			if(A1_detect_one_chain(&cfg))
				no_cgpu++;
		}
	}

#endif //USE_GPIO_CS 


	if (no_cgpu==0) {
		applog(LOG_ERR, "No any A1 board");
		pabort("\nNo any A1 board\n");
	}
	else 
	{
		struct cgpu_info *cgpu;
		struct A1_chain *a1;
		uint32_t num_cores=0,eff;
		float speed;
		int i;
		for(i=0;i<no_cgpu;i++)
		{
			cgpu = get_devices(i);
			a1 = cgpu->device_data;
			num_cores+=a1->num_cores;
		}
		eff=((num_cores*100)/(no_cgpu*MAX_CORE_NUMS*MAX_ASIC_NUMS));
		speed=(no_cgpu*eff*A1_MAX_SPEED*PLL_Clk_12Mhz[A1Pll1].speedMHz/1000)/100.0;
		applog(LOG_NOTICE, "A1 boards=%d, active cores=%d, Efficient=%d%%, speed=%.1fM",no_cgpu,num_cores,eff,speed);
	}



}

/* return value is nonces processed since previous call */
void A1_DetectOverHeat(struct A1_chain *a1,int chip)
{
	int i,temp;
	struct Alarm *alarm;
	struct timeval tv_now; //alarm start time

	temp=a1->chips[chip].temp;
	for(i=0;i<2;i++)
	{
		alarm=&a1->chips[chip].alarm[i];
		if(temp>=alarm->cutoffTemp)
		{
			cgtime(&tv_now); //now
			if((tv_now.tv_sec-alarm->tv_start.tv_sec)>=alarm->timeout)
			{
				alarm->overheat=1;
			}
		}
		else
		{
			alarm->overheat=0;
			 cgtime(&alarm->tv_start); //reset timer

		}

	}
}

/* return value is nonces processed since previous call */
void A1_CheckOverHeatAlarm(struct A1_chain *a1)
{
	int i,alarmCnt=0;
#ifdef A1_SHUTDOWN_ASIC_BD_1
	for(i=0;i<a1->num_chips;i++)
	{
		if(a1->chips[i].alarm[0].overheat)
		{
			a1->status|=ERROR_OVERHEAD;
			a1->shutdown=1;
			break;
		}
	}
#endif
#ifdef A1_SHUTDOWN_ASIC_BD_2
	for(i=0;i<a1->num_chips;i++)
	{
		if(a1->chips[i].alarm[1].overheat)
		{
			if(alarmCnt)
			{
				a1->status|=ERROR_OVERHEAD;
				a1->shutdown=2;
				break;
			}
			alarmCnt++;
		}
	}
#endif	

	if(a1->shutdown)
		applog(LOG_ERR, "ASIC board cs=%d shutdown due to overheat",a1->spi_ctx->config.gpio_cs);


}

static int64_t A1_scanwork(struct thr_info *thr)
{
	int i,j;
	struct cgpu_info *cgpu = thr->cgpu;
	struct A1_chain *a1 = cgpu->device_data;
	int32_t nonce_cnt = 0;
	int32_t A1Pll = 1000;
	int32_t nonce_ranges_processed = 0;

//	applog(LOG_ERR, "A1_scanwork cs=%d",a1->spi_ctx->config.gpio_cs);

	uint32_t nonce;
	uint8_t chip_id;
	uint8_t job_id,evtKey;
	bool work_updated = false,overheat=false;
#if 0
	switch(KeyDetect())
	{
		case KEY_EVENT_RESET:
			A1_reinit();
			break;
	}
#endif

	mutex_lock(&a1->lock);
#ifdef USE_ST_MCU	
	if(cgpu->restart)
	{
		A1_reinit_device(cgpu);
		cgpu->restart=0;
		mutex_unlock(&a1->lock);
		return 0;
	}
#endif
	if(a1->chips == NULL||a1->shutdown) //not need post job as not chip avaiable
	{
		cgsleep_ms(100);/* in case of no progress, prevent busy looping */
		mutex_unlock(&a1->lock);
		return 0;
	}

//        if(a1->reset_bc == 0) {
//		if(a1->hashes_times > 600) {
//                        //A1_flush_work_unlock(cgpu);
//                        a1->reset_bc = 1;
//                        a1->hashes_times = 0;
//                        applog(LOG_NOTICE, "Job is working over 60s, flushing work, chip(cs%d) %d         ",a1->spi_ctx->config.gpio_cs,  i);
//                }
//                else {
//                        a1->hashes_times++;
//                }
//	}

        if(a1->reset_bc == 0) {
		a1->num_cores = 0;

		for (i = 0; i < a1->num_chips; i++) {
			if(a1->chips[i].num_cores==0) //No any core can do work in this chip
				continue;

			if (!cmd_READ_REG(a1, i + 1)) {
				applog(LOG_ERR, "Failed to read reg from chip(cs%d) %d            ",a1->spi_ctx->config.gpio_cs,  i);
				continue;
			}

			if ((a1->spi_rx[11] & 0x01) != 0x01) { // not working 
				a1->reset_bc = 1;
				a1->chips[i].reset_errors++;
				applog(LOG_ERR, "Job is not working from chip(cs%d) %d          ",a1->spi_ctx->config.gpio_cs,  i);
			}
			else
			{
				a1->chips[i].reset_errors = 0;
			}

			if (a1->chips[i].reset_errors > 2){
				a1->chips[i].num_cores = 0;
				a1->chips[i].reset_errors = 0;
				a1->reinit = 1;
				applog(LOG_ERR, "Job it not working for many times chip(cs%d) %d  ",a1->spi_ctx->config.gpio_cs,  i);
			}			

			a1->num_cores += a1->chips[i].num_cores;
		}
	}

        if(a1->reset_bc == 1)
        {
		a1->reset_bc = 0;
		a1->hashes_times = 0;
		cgtime(&a1->tvScryptLast);
		cgtime(&a1->tvScryptCurr);

		cmd_RESETBC_BCAST(a1);
		cmd_RESETBC_BCAST(a1);
		cmd_RESETBC_BCAST(a1);
		cmd_RESETBC_BCAST(a1);

		while (get_nonce(a1, (uint8_t*)&nonce, &chip_id, &job_id)) {
			nonce_cnt++;
                	if (nonce_cnt > 8){
				a1->reinit = 1;
                        	applog(LOG_ERR, "(cs%d) reset bc get nonce deadlock              ",a1->spi_ctx->config.gpio_cs);
				break;
                	}
		}

		for (i = 0; i < a1->num_chips; i++) {
			if(a1->chips[i].num_cores==0) //No any core can do work in this chip
				continue;

			if (!cmd_READ_REG(a1, i + 1)) {
				applog(LOG_ERR, "Failed to read reg from chip(cs%d) %d           ",a1->spi_ctx->config.gpio_cs,  i);
				continue;
			}

			if ((a1->spi_rx[11] & 0x01) == 0x01) { // reset bc fail ?
				a1->reinit = 1;
				applog(LOG_ERR, "Failed to reset bc from chip(cs%d) %d           ",a1->spi_ctx->config.gpio_cs,  i);
			}
		}
        }

	if(a1->reinit == 1)
	{
		A1_reinit_device(cgpu);
		a1->reinit = 0;
		cgsleep_ms(1000);
		mutex_unlock(&a1->lock);
		return 0;
	}

	/* poll queued results */
	while (get_nonce(a1, (uint8_t*)&nonce, &chip_id, &job_id)) {
		//nonce = bswap_32(nonce);		// modified for a2
		work_updated = true;
		nonce_cnt++;

		if (nonce_cnt > 8){
			a1->reset_bc = 1;
			applog(LOG_ERR, "(cs%d) get nonce deadlock",a1->spi_ctx->config.gpio_cs);
			cgsleep_ms(100);
			mutex_unlock(&a1->lock);
                	return 0;
		}
#if 1
		if(chip_id == 0 ||chip_id > a1->num_chips)
			continue;
		if(job_id == 0 ||job_id>4 )
			continue;
#else
		assert(chip_id > 0 && chip_id <= a1->num_chips);
		assert(job_id > 0 && job_id <= 4);
#endif		

		struct A1_chip *chip = &a1->chips[chip_id - 1];
		struct work *work = chip->work[job_id - 1];
		if (work == NULL) {
			/* already been flushed => stale */
			a1->reset_bc = 1;
			applog(LOG_ERR, "chip(cs%d) %d: stale nonce 0x%08x                                       ",
			       a1->spi_ctx->config.gpio_cs,chip_id, nonce);
			chip->stales++;
			cgsleep_ms(100);
			mutex_unlock(&a1->lock);
			return 0;
		}
		if (!submit_nonce(thr, work, nonce)) {
			if(opt_realquiet)
				applog(LOG_ERR, "chip(cs%d) %d: invalid nonce 0x%08x                                 ", 
			       a1->spi_ctx->config.gpio_cs,chip_id, nonce);
			else				
				applog(LOG_ERR, "chip(cs%d) %d: invalid nonce 0x%08x                                 ", 
			       a1->spi_ctx->config.gpio_cs,chip_id, nonce);
			chip->hw_errors++;
			/* add a penalty of a full nonce range on HW errors */
			nonce_ranges_processed--;
			continue;
		}
		applog(LOG_DEBUG, "YEAH: chip(cs%d) %d: job %d nonce 0x%08x",
		       a1->spi_ctx->config.gpio_cs,chip_id,job_id,nonce);
		chip->nonces_found++;
	}

	applog(LOG_DEBUG, "chip(cs%d) nonce_cnt = %d", a1->spi_ctx->config.gpio_cs, nonce_cnt);

	/* check for completed works */
	for (i = 0; i < a1->num_chips; i++) {
#if 1 //Patch D
		if(a1->chips[i].num_cores==0) //No any core can do work in this chip
			continue;
#endif

		if (!cmd_READ_REG(a1, i + 1)) {
			applog(LOG_ERR, "Failed to read reg from chip(cs%d) %d",a1->spi_ctx->config.gpio_cs,  i);
			// TODO: what to do now?
			continue;
		}

		if(a1->spi_rx[13] == 0){ // a1->chips[i].num_cores
			a1->reset_bc = 1;
			applog(LOG_ERR, "Failed to read num_cores from chip(cs%d) %d",a1->spi_ctx->config.gpio_cs,  i);
			cgsleep_ms(100);
			mutex_unlock(&a1->lock);
                	return 0;
		}

		hexdump("A1 RX", a1->spi_rx, 8);
#ifdef A1_TEMP_EN	
		//a1->chips[i].temp= (a1->spi_rx[8]<<8)|a1->spi_rx[9];
		a1->chips[i].temp= (a1->spi_rx[14]<<8)|a1->spi_rx[15];	// modified for a2
#endif
		//if ((a1->spi_rx[5] & 0x02) != 0x02) {
		if ((a1->spi_rx[11] & 0x01) != 0x01) {					// modified for a2
			applog(LOG_DEBUG, "get_work...");
			work_updated = true;
			struct work *work = wq_dequeue(&a1->active_wq);
			assert(work != NULL);

#ifdef A1_TEMP_EN	
		if(a1->cutTemp>0)
		{
		
			A1_DetectOverHeat(a1,i);
		
#ifdef A1_CUTOFF_TEMP_CHIP			
			overheat=false;
#if 1 // if current ASIC is higher then cutoff temperature, don't setwork
			if(a1->cutTemp<a1->chips[i].temp)
				continue;
#else // if any one ASIC is higher then cutoff temperature, don't setwork
			for (j = 0; j < a1->num_chips; j++) {
				if(a1->cutTemp<a1->chips[j].temp)
				{
					overheat=true;
					break;
				}
			}
			if(overheat)
				continue;
#endif
#endif
		}
#endif
			if (!set_work(a1, i + 1, work))
				continue;

			nonce_ranges_processed++;
			struct A1_chip *chip = &a1->chips[i];
			chip->nonce_ranges_done++;
#if 0 // not need			
			applog(LOG_NOTICE, "chip(cs%d) %d: job done => %d/%d/%d/%d",
			       a1->spi_ctx->config.gpio_cs,i + 1,
			       chip->nonce_ranges_done, chip->nonces_found,
			       chip->hw_errors, chip->stales);
#endif
		}
	}

	mutex_unlock(&a1->lock);

	if (nonce_ranges_processed < 0) {
		applog(LOG_INFO, "Negative nonce_processed");
		nonce_ranges_processed = 0;
	}

	if (nonce_ranges_processed != 0) {
		applog(LOG_INFO, "nonces processed %d",nonce_ranges_processed);
	}

	cgtime(&a1->tvScryptCurr);
	timersub(&a1->tvScryptCurr, &a1->tvScryptLast, &a1->tvScryptDiff);
	cgtime(&a1->tvScryptLast);

	/* in case of no progress, prevent busy looping */
	//if (!work_updated)
		cgsleep_ms(100);

	//applog(LOG_NOTICE, "second %d, usecond %d",a1->tvScryptDiff.tv_sec,a1->tvScryptDiff.tv_usec);

	if(nonce_ranges_processed==-1) //prevent A1 board disable
		nonce_ranges_processed =0;

	if(a1->cutTemp>0)
		A1_CheckOverHeatAlarm(a1);

	switch(cgpu->device_id){
		case 0:A1Pll = PLL_Clk_12Mhz[A1Pll1].speedMHz;break;
		case 1:A1Pll = PLL_Clk_12Mhz[A1Pll2].speedMHz;break;
		case 2:A1Pll = PLL_Clk_12Mhz[A1Pll3].speedMHz;break;
		case 3:A1Pll = PLL_Clk_12Mhz[A1Pll4].speedMHz;break;
		case 4:A1Pll = PLL_Clk_12Mhz[A1Pll5].speedMHz;break;
		case 5:A1Pll = PLL_Clk_12Mhz[A1Pll6].speedMHz;break;
		default:;
	}

	return (int64_t)(1550832.85 * A1Pll/1000 * (a1->num_cores/54.0) * (a1->tvScryptDiff.tv_usec / 1000000.0));
}

/* queue one work per chip in chain */
static bool A1_queue_full(struct cgpu_info *cgpu)
{
	struct A1_chain *a1 = cgpu->device_data;
	int queue_full = false;
	struct work *work;

	mutex_lock(&a1->lock);
	applog(LOG_DEBUG, "A1 running queue_full: %d/%d",
	       a1->active_wq.num_elems, a1->num_chips);

	if (a1->active_wq.num_elems >= a1->num_chips) {
		applog(LOG_DEBUG, "active_wq full");
		queue_full = true;
	} else {
		wq_enqueue(&a1->active_wq, get_queued(cgpu));
	}
	mutex_unlock(&a1->lock);

	return queue_full;
}

void A1_flush_work(struct cgpu_info *cgpu)
{
	struct A1_chain *a1 = cgpu->device_data;
	applog(LOG_DEBUG, "A1 running flushwork");

	int i;

	if(a1->chips == NULL)
		return;

	mutex_lock(&a1->lock);
	/* stop chips hashing current work */
	if (!abort_work(a1)) {
		applog(LOG_ERR, "failed to abort work in chip chain!");
	}
	/* flush the work chips were currently hashing */
	for (i = 0; i < a1->num_chips; i++) {
		int j;
		struct A1_chip *chip = &a1->chips[i];
		for (j = 0; j < 4; j++) {
			struct work *work = chip->work[j];
			if (work == NULL)
				continue;
			applog(LOG_DEBUG, "flushing chip %d, work %d: 0x%p",
			       i, j + 1, work);
			work_completed(cgpu, work);
			chip->work[j] = NULL;
		}
	}
	/* flush queued work */
	applog(LOG_DEBUG, "flushing queued work...");
	while (a1->active_wq.num_elems > 0) {
		struct work *work = wq_dequeue(&a1->active_wq);
		assert(work != NULL);
		applog(LOG_DEBUG, "flushing 0x%p", work);
		work_completed(cgpu, work);
	}
	mutex_unlock(&a1->lock);
}

void A1_flush_work_unlock(struct cgpu_info *cgpu)
{
	struct A1_chain *a1 = cgpu->device_data;
	applog(LOG_DEBUG, "A1 running flushwork");

	int i;

	if(a1->chips == NULL)
		return;

	/* stop chips hashing current work */
	if (!abort_work(a1)) {
		applog(LOG_ERR, "failed to abort work in chip chain!");
	}
	/* flush the work chips were currently hashing */
	for (i = 0; i < a1->num_chips; i++) {
		int j;
		struct A1_chip *chip = &a1->chips[i];
		for (j = 0; j < 4; j++) {
			struct work *work = chip->work[j];
			if (work == NULL)
				continue;
			applog(LOG_DEBUG, "flushing chip %d, work %d: 0x%p",
			       i, j + 1, work);
			work_completed(cgpu, work);
			chip->work[j] = NULL;
		}
	}
	/* flush queued work */
	applog(LOG_DEBUG, "flushing queued work...");
	while (a1->active_wq.num_elems > 0) {
		struct work *work = wq_dequeue(&a1->active_wq);
		assert(work != NULL);
		applog(LOG_DEBUG, "flushing 0x%p", work);
		work_completed(cgpu, work);
	}
}

#ifdef API_STATE_EN
static void A1_reinit_device(struct cgpu_info *cgpu)
{
	struct A1_chain *a1 = cgpu->device_data;

	a1->reset_bc = 1;
	a1->status = ASIC_BOARD_OK;

	applog(LOG_NOTICE, "Reinit board (cs%d)", a1->spi_ctx->config.gpio_cs);
	int i,num_chips =0;

#ifdef ST_MCU_EN
	if (!cmd_RESET_BCAST(a1))
		goto failure;

	if (!cmd_BIST_START(a1))
		goto failure;
#else
	if (!cmd_RESET_BCAST(a1) || !cmd_BIST_START(a1))
		goto failure;
#endif
	a1->num_chips = a1->spi_rx[3];
	applog(LOG_WARNING, "spidev%d.%d(cs%d): Found %d A1 chips",
	       a1->spi_ctx->config.bus, a1->spi_ctx->config.cs_line,
	       a1->spi_ctx->config.gpio_cs,a1->num_chips);

	a1->num_cores=0;

	if (a1->num_chips == 0)
		goto failure;

	if (!cmd_BIST_COLLECT_BCAST(a1))	// modified for a2
		goto failure;

	if (!cmd_BIST_FIX_BCAST(a1))
		goto failure;

	if(a1->num_chips<MAX_ASIC_NUMS)
		a1->status|=ERROR_CHIP;

	for (i = 0; i < a1->num_chips; i++) {
		int chip_id = i + 1;
		if (!cmd_READ_REG(a1, chip_id)) {
			applog(LOG_WARNING, "Failed to read register for "
			       "chip %d -> disabling", chip_id);
			a1->chips[i].num_cores = 0;
			continue;
		}
		num_chips++;
		a1->chips[i].num_cores = a1->spi_rx[13];		// modified for a2
		if(a1->chips[i].num_cores>MAX_CORE_NUMS)		// modified for a2
			a1->chips[i].num_cores = MAX_CORE_NUMS;

		if(a1->chips[i].num_cores<MAX_CORE_NUMS)
			a1->status=ERROR_CORE;

		memcpy(a1->chips[i].reg, a1->spi_rx + 2, 12);	//keep ASIC register value	// modified for a2
#ifdef A1_TEMP_EN	
		a1->chips[i].temp= (a1->spi_rx[14]<<8)|a1->spi_rx[15];	// modified for a2
#endif

#ifdef A1_SHUTDOWN_ASIC_BD_1
		a1->chips[i].alarm[0].overheat=0;
		a1->chips[i].alarm[0].timeout=5;
		a1->chips[i].alarm[0].cutoffTemp=stTempCut+15;
#endif
		
#ifdef A1_SHUTDOWN_ASIC_BD_2
		a1->chips[i].alarm[1].overheat=0;
		a1->chips[i].alarm[1].timeout=5;
		a1->chips[i].alarm[1].cutoffTemp=stTempCut+10;
#endif

		a1->num_cores += a1->chips[i].num_cores;
		applog(LOG_WARNING, "Found chip %d with %d active cores",
		       chip_id, a1->chips[i].num_cores);

		a1->chips[i].reset_errors = 0;
	}
	applog(LOG_WARNING, "Found %d chips with total %d active cores",
	       a1->num_chips, a1->num_cores);

	if(a1->num_cores==0) // The A1 board  haven't any cores
		goto failure;		

	switch(cgpu->device_id){
		case 0:A1_SetA1PLLClock(a1, A1Pll1);break;
		case 1:A1_SetA1PLLClock(a1, A1Pll2);break;
		case 2:A1_SetA1PLLClock(a1, A1Pll3);break;
		case 3:A1_SetA1PLLClock(a1, A1Pll4);break;
		case 4:A1_SetA1PLLClock(a1, A1Pll5);break;
		case 5:A1_SetA1PLLClock(a1, A1Pll6);break;
		default:;
	}

	return;

failure:
	applog(LOG_NOTICE, "Reinit board failure (cs%d)", a1->spi_ctx->config.gpio_cs);

	// Set zero cores before check num chips
	for (i = 0; i < a1->num_chips; i++)
		a1->chips[i].num_cores = 0;
	a1->num_cores = 0;

	return;
}

void A1_reinit(void)
{
	int i;
	struct A1_chain *a1;
	struct cgpu_info *cgpu;

	applog(LOG_WARNING, "A1_reinit");

	testFirstTime=false;


	rd_lock(&devices_lock);
	for (i = 0; i < total_devices; ++i)
	{
		cgpu=devices[i];
		a1=cgpu->device_data;

		applog(LOG_WARNING, "%d,%d,%d",cgpu->deven,cgpu->status,a1->status);
		cgpu->deven = DEV_RECOVER;
//		a1->reinit=1;
		cgpu->restart=1;

	}
	rd_unlock(&devices_lock);
	

}
void A1_shutdown(void)
{
	int i;
	struct A1_chain *a1;
	struct cgpu_info *cgpu;

	applog(LOG_WARNING, "A1_shutdown");

	testFirstTime=true;

	rd_lock(&devices_lock);
	for (i = 0; i < total_devices; ++i)
	{
		cgpu=devices[i];
		a1=cgpu->device_data;
		a1->shutdown=1;
	}
	rd_unlock(&devices_lock);
	

}

static void A1_get_statline(char *buf, size_t bufsiz, struct cgpu_info *cgpu)
{
	struct A1_chain *a1 = cgpu->device_data;
	int i;
	char strT[100],*pStrT;
	float avgTemp=0,maxTemp=0;
	bool overheat=false;

	// Warning, access to these is not locked - but we don't really
	// care since hashing performance is way more important than
	// locking access to displaying API debug 'stats'
	// If locking becomes an issue for any of them, use copy_data=true also


	if (a1->chips == NULL) 
	{
//		tailsprintf(buf, bufsiz, " | T:--C");
		return;
	}

	if(cgpu->deven != DEV_ENABLED)
		return;


	avgTemp=a1->chips[0].temp;
	pStrT=strT;
	sprintf(pStrT,"%02d",a1->chips[0].temp);
	pStrT+=2;


	for(i=1;i<a1->num_chips;i++)
	{
		if(maxTemp<a1->chips[i].temp)
			maxTemp=a1->chips[i].temp;

		if(a1->cutTemp<a1->chips[i].temp)
			overheat=true;
			
		avgTemp+=a1->chips[i].temp;
		sprintf(pStrT,"-%02d",a1->chips[i].temp);		
		pStrT+=3;

	}
	avgTemp/=a1->num_chips;

	if(a1->shutdown)
		tailsprintf(buf, bufsiz, " | T:%2.0fC(%s)(shutdown=%d)", maxTemp,strT,a1->shutdown);
	else if(overheat)
		tailsprintf(buf, bufsiz, " | T:%2.0fC*Hot*(%s)", maxTemp,strT);
	else	
		tailsprintf(buf, bufsiz, " | T:%2.0fC     (%s)", maxTemp,strT);


}

static void A1_statline_before(char *buf, size_t bufsiz, struct cgpu_info *cgpu)
{
	struct A1_chain *a1 = cgpu->device_data;
	char strT[100],*pStrT;
	int i;
	
	if (a1->chips == NULL) 
		return;

	if(cgpu->deven != DEV_ENABLED)
		return;



#ifdef USE_GPIO_CS	
#if 1
	pStrT=strT;

	sprintf(pStrT,"cs:%d A:%d");

	if(a1->num_chips<MAX_ASIC_NUMS)
		sprintf(pStrT,"cs:%d A:%d*-C:%03d ", a1->spi_ctx->config.gpio_cs, a1->num_chips, a1->num_cores);
	else if(a1->num_cores<MAX_CORE_NUMS*MAX_ASIC_NUMS)
		sprintf(pStrT,"cs:%d A:%d -C:%03d*", a1->spi_ctx->config.gpio_cs, a1->num_chips, a1->num_cores);
	else
		sprintf(pStrT,"cs:%d A:%d -C:%03d ", a1->spi_ctx->config.gpio_cs, a1->num_chips, a1->num_cores);

	pStrT+=15;
	if(a1->num_chips)
	{
		sprintf(pStrT," (%02d", a1->chips[i].num_cores);
		pStrT+=4;

		for(i=1;i<a1->num_chips;i++)
		{
				sprintf(pStrT,"-%02d", a1->chips[i].num_cores);
				pStrT+=3;
		}
		sprintf(pStrT,")");
		pStrT+=1;
	}
	sprintf(pStrT," | ");
	pStrT+=3;

	tailsprintf(buf, bufsiz, "%s", strT);

#else
		if(a1->num_chips<MAX_ASIC_NUMS)
			tailsprintf(buf, bufsiz, "cs:%d A:%dE-C:%03d | ", a1->spi_ctx->config.gpio_cs, a1->num_chips, a1->num_cores);
		else if(a1->num_cores<MAX_CORE_NUMS)
			tailsprintf(buf, bufsiz, "cs:%d A:%d-C:%03dE | ", a1->spi_ctx->config.gpio_cs, a1->num_chips, a1->num_cores);
		else
			tailsprintf(buf, bufsiz, "cs:%d A:%d-C:%03d | ", a1->spi_ctx->config.gpio_cs, a1->num_chips, a1->num_cores);
#endif		
//	}

#else
#ifdef A1_TEST_MODE_EN
	if(testMode>0)
	{
		if(a1->num_chips<MAX_ASIC_NUMS)
			tailsprintf(buf, bufsiz, "A:%dE-C:%03d | ", a1->num_chips, a1->num_cores);
		else if(a1->num_cores<MAX_CORE_NUMS)
			tailsprintf(buf, bufsiz, "A:%d-C:%03dE | ", a1->num_chips, a1->num_cores);
		else
			tailsprintf(buf, bufsiz, "A:%d-C:%03d | ", a1->num_chips, a1->num_cores);
	}
	else
#endif
	{
		tailsprintf(buf, bufsiz, "A:%d-C:%03d | ", a1->num_chips, a1->num_cores);
	}
#endif
}

#ifdef API_STATE_EN
static struct api_data *A1_api_stats(struct cgpu_info *cgpu)
{
	struct api_data *root = NULL;
	struct A1_chain *a1 = cgpu->device_data;
	int i;
	char strT[100],*pStrT;
	float avgTemp=0;

	if (a1->chips == NULL) 
	{
		root = api_add_string(root, "Temperature", strT, false);
		return root ;
	}

	// Warning, access to these is not locked - but we don't really
	// care since hashing performance is way more important than
	// locking access to displaying API debug 'stats'
	// If locking becomes an issue for any of them, use copy_data=true also
#ifdef USE_GPIO_CS	
	root = api_add_int(root, "CS", (int*)&(a1->spi_ctx->config.gpio_cs), false);
#endif
	root = api_add_int(root, "ASIC", &(a1->num_chips), false);
	root = api_add_int(root, "CORES(TOTAL)", &(a1->num_cores), false);

	pStrT=strT;
        sprintf(pStrT,"%02d",a1->chips[0].num_cores);
        pStrT+=2;

        for(i=1;i<a1->num_chips;i++)
        {
                if(a1->chips[i].num_cores)
                {
                        sprintf(pStrT,"-%02d",a1->chips[i].num_cores);
                }
                else
                        sprintf(pStrT,"- F");

                pStrT+=3;
        }

	root = api_add_string(root, "CORES(SOLO)", strT, true);


	pStrT=strT;
	sprintf(pStrT,"%02d",a1->chips[0].temp);
	pStrT+=2;

	for(i=1;i<a1->num_chips;i++)
	{
		if(a1->chips[i].temp)
		{
			avgTemp+=a1->chips[i].temp;
			sprintf(pStrT,"-%02d",a1->chips[i].temp);
		}
		else
			sprintf(pStrT,"- F");
		
		pStrT+=3;
	}
	avgTemp/=a1->num_chips;
	root = api_add_temp(root, "TEMP(AVG)", &avgTemp, true);
	root = api_add_string(root, "TEMP(SOLO)", strT, true);

	return root;
}
#endif

#ifdef HAVE_CURSES

#define CURBUFSIZ 256
#define cg_mvwprintw(win, y, x, fmt, ...) do { \
	char tmp42[CURBUFSIZ]; \
	snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
	mvwprintw(win, y, x, "%s", tmp42); \
} while (0)
#define cg_wprintw(win, fmt, ...) do { \
	char tmp42[CURBUFSIZ]; \
	snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
	wprintw(win, "%s", tmp42); \
} while (0)


extern WINDOW *mainwin, *statuswin, *logwin;


void A1_curses_print_status(int y)
{

//	applog(LOG_WARNING, "A1_curses_print_status");
	struct A1_chain *a1;
	struct cgpu_info *cgpu;
	int i,testing=0,errHw=0,errBoard=0,errOverheat=0,errChip=0,errCore=0,errSenor=0,numChips=0;


	char strT[100],*pStrT;

	if(testFirstTime)
	{
		y+=2;
		cg_mvwprintw(statuswin, y, 1, "Press 'R' key for hardware testing");
		return;
	}


#if 1
	//Note: not allow write any data to  devices[i]
	rd_lock(&devices_lock);
	for (i = 0; i < total_devices; ++i)
	{
		cgpu=devices[i];
		a1=cgpu->device_data;

		if(cgpu->deven == DEV_RECOVER)
		{
			testing=1;
			break;
		}
		if(a1->status&(ERROR_BOARD|ERROR_CHIP|ERROR_CORE|ERROR_TEMP_SENSOR))
		{
			errHw=1;

			if(a1->status&ERROR_CHIP)
				errChip=1;
			if(a1->status&ERROR_CORE)
				errCore=1;
			if(a1->status&ERROR_TEMP_SENSOR)
				errSenor=1;
			if(a1->status&ERROR_BOARD)
				errBoard=1;
			if(a1->status&ERROR_OVERHEAD)
				errOverheat=1;
		}

		

		if(a1->status==ASIC_BOARD_OK)
		{
			numChips++;
		}

	}
	rd_unlock(&devices_lock);
#endif

	if(numChips<MAX_ASIC_BOARD)
	{
//		errHw=1;
	//	errBoard=1;
	}

	++y;
	mvwhline(statuswin, y, 0, '-', 80);

	if(total_devices)
	{
		if(testing)
			cg_mvwprintw(statuswin, ++y, 1, "Testing .....");
		else if(errHw)
		{
			pStrT=strT;
			sprintf(pStrT,"Hardware: ");
			pStrT+=strlen("Hardware: ");			
			if(errBoard)
			{
				sprintf(pStrT,"ASIC board error,");
				pStrT+=strlen("ASIC board error,");
			}
			if(errChip)
			{
				sprintf(pStrT,"Chip error,");
				pStrT+=strlen("Chip error,");
			}

			if(errCore)
			{
				sprintf(pStrT,"Core error,");
				pStrT+=strlen("Core error,");
			}

			if(errSenor)
			{
				sprintf(pStrT,"Senor error,");
				pStrT+=strlen("Senor error,");
			}

			if(errOverheat)
			{
				sprintf(pStrT,"Overheat");
				pStrT+=strlen("Overheat");
			}

			cg_mvwprintw(statuswin, ++y, 1, "%s",strT);
		}
		else
			cg_mvwprintw(statuswin, ++y, 1, "Hardware testing: Passed");	
	}
	else
		cg_mvwprintw(statuswin, ++y, 1, "Test finished: No any ASIC board");	

	wclrtoeol(statuswin);
	++y;
	mvwhline(statuswin, y, 0, '-', 80);
	//wattroff(statuswin, A_BOLD);
	wclrtoeol(statuswin);
}

#endif

void A1_stop_test(struct cgpu_info *cgpu)
{
	struct A1_chain *a1 = cgpu->device_data;
//    a1->status  = (a1->status & (~ASIC_BOARD_TESTING));
}
static bool A1_get_stats(struct cgpu_info *cgpu)
{

	struct A1_chain *a1 = cgpu->device_data;
	int i,overheatCnt;
	char strT[100],*pStrT;
	float avgTemp=0,maxTemp=0;
	bool overheat=false;

	// Warning, access to these is not locked - but we don't really
	// care since hashing performance is way more important than
	// locking access to displaying API debug 'stats'
	// If locking becomes an issue for any of them, use copy_data=true also

	if (a1->chips == NULL) 
	{
//		cgpu->deven=DEV_DISABLED;
		//cgpu->status= LIFE_NOSTART;
//		a1->status=NO_ASIC_BOARD;
		return true;
	}


	if(a1->num_chips<MAX_ASIC_NUMS)
	{
//		cgpu->status = LIFE_DEAD;
		a1->status|=ERROR_CHIP;
	}
	else
	{
		if(a1->num_cores<(MAX_ASIC_NUMS*MAX_CORE_NUMS))
		{
//			cgpu->status = LIFE_SICK;
			a1->status|=ERROR_CORE;
		}

		for(i=0;i<a1->num_chips;i++)
		{
			if(a1->chips[i].temp==0)
			{
//				cgpu->status = LIFE_SICK;
				a1->status|=ERROR_TEMP_SENSOR;				
			}
		}
	}

/* Check temperature, then stop if
	2) For two or more asic chip has temperature higher than the argu value for 5 seconds, it stop submitting job for the whole board forever.
	
	3) For only one chip has temperature 10 degree higher than the argu value for 5 seconds, it stop submitting job for the whole board forever.
	
	4) even only one chip suddenly 15 degree higher than the argu value, it stop immediately.
*/

#if 0
#ifdef A1_SHUTDOWN_ASIC_BD_1
		for(i=0;i<a1->num_chips;i++)
		{
			if(a1->chips[i].temp>=(a1->cutTemp+15))
			{
				a1->shutdown=1;
				break;
			}
	
		}
	
#endif

#ifdef A1_SHUTDOWN_ASIC_BD_2 
		overheatCnt=0;
		for(i=0;i<a1->num_chips;i++)
		{
			if(a1->chips[i].temp>=a1->cutTemp+10)
			{
				if (overheatCnt) 
				{
					a1->shutdown=2;
					break;
				}
				overheatCnt++;
			}
		}
	
#endif
#endif
	return true;
}
#endif
struct device_drv bitmineA1_drv = {
	.drv_id = DRIVER_bitmineA1,
	.dname = "BA1",
	.name = "BA1",
	.drv_detect = A1_detect,

	.hash_work = hash_queued_work,
	.scanwork = A1_scanwork,
	.queue_full = A1_queue_full,
	.flush_work = A1_flush_work,
	.reinit_device = A1_reinit_device,
#ifdef USE_ST_MCU
	.get_statline_before = A1_statline_before,	
	.get_statline= A1_get_statline,	
	.get_stats= A1_get_stats,
#ifdef API_STATE_EN
	.get_api_stats = A1_api_stats,
#endif	
#endif	
};
